// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License

/**
 * The renderer API is exposed by the preload script found in the preload.ts
 * file in order to give the renderer access to the Node API in a secure and 
 * controlled way
 */
const welcomeDiv = document.getElementById('WelcomeMessage');
const signInButton = document.getElementById('signIn');
const cardDiv = document.getElementById('cardDiv');
const profileDiv = document.getElementById('profileDiv');

window.renderer.showWelcomeMessage((event, account) => {
    if (!account) return;

    cardDiv.style.display = 'initial';
    welcomeDiv.innerHTML = `Welcome ${account.name}`;
});

window.renderer.handleProfileData((event, graphResponse) => {
    if (!graphResponse) return;

    console.log(`Graph API responded at: ${new Date().toString()}`);
    getCalendarEvents_func(graphResponse);
    //setProfile(graphResponse);
});

// UI event handlers
signInButton.addEventListener('click', () => {
    window.renderer.sendLoginMessage();
});


const setProfile = (data) => {
    if (!data) return;
    
    profileDiv.innerHTML = '';

    const title = document.createElement('p');
    const email = document.createElement('p');
    const phone = document.createElement('p');
    const address = document.createElement('p');

    title.innerHTML = '<strong>Name: </strong>' + data.displayName;
    email.innerHTML = '<strong>Mail: </strong>' + data.userPrincipalName;
    phone.innerHTML = '<strong>Surname: </strong>' + data.surname;
    address.innerHTML = '<strong>First Name: </strong>' + data.givenName;

    profileDiv.appendChild(title);
    profileDiv.appendChild(email);
    profileDiv.appendChild(phone);
    profileDiv.appendChild(address);
}

const getCalendarEvents_func = (data) => {
    if (!data) return;
    
    profileDiv.innerHTML = '';
    
    const events = document.createElement('p');
    const startTime = document.createElement('p');
    const endTime = document.createElement('p');
    const attendees_ = document.createElement('p');
    const location = document.createElement('p');
    
    const emails = data.value[0].attendees.map((attendee) => attendee.emailAddress.name);

    events.innerHTML = '<strong>Events: </strong>' + data.value[0].subject;
    startTime.innerHTML = '<strong>Start Time: </strong>' + data.value[0].start.dateTime;
    endTime.innerHTML = '<strong>End Time: </strong>' + data.value[0].end.dateTime;
    attendees_.innerHTML = '<strong>Attendees: </strong>' + emails;
    location.innerHTML = '<strong>Location: </strong>' + data.value[0].location.displayName;

    // console.log(data);

    profileDiv.appendChild(events);
    //console.log(data.value[0].subject);
    profileDiv.appendChild(startTime);
    profileDiv.appendChild(endTime);
    profileDiv.appendChild(attendees_);
    profileDiv.appendChild(location);

}