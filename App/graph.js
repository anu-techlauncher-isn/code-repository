const { Client } = require('@microsoft/microsoft-graph-client');
require('isomorphic-fetch');

/**
 * Creating a Graph client instance via options method. For more information, visit:
 * https://github.com/microsoftgraph/msgraph-sdk-javascript/blob/dev/docs/CreatingClientInstance.md#2-create-with-options
 * @param {String} accessToken
 * @returns
 */
const getGraphClient = (accessToken) => {
    // Initialize Graph client
    const graphClient = Client.init({
        // Use the provided access token to authenticate requests
        authProvider: (done) => {
            done(null, accessToken);
        },
    });

    return graphClient;
};

const getCalendarEvents = async (accessToken) => {
    const graphClient = getGraphClient(accessToken);

    const events = await graphClient
        .api('/me/calendar/events')
        .select('subject,start,end')
        .get();

    console.log(events.value);
    return events;
};

//const graphClient = MicrosoftGraph.Client.initWithMiddleware({ authProvider });
// async function getUser() {
//     ensureScope('user.read');
//     return await graphClient
//         .api('/me')
//         .select('id,displayName')
//         .get();
// }

// async function getEvents() {
//     ensureScope('Calendars.Read');
//     const dateNow = new Date();
//     const dateNextWeek = new Date();
//     dateNextWeek.setDate(dateNextWeek.getDate() + 7);
//     const query = `startDateTime=${dateNow.toISOString()}&endDateTime=${dateNextWeek.toISOString()}`;

  
//     return await graphClient
//         .api('/me/calendar/events').query(query)
//         .select('subject,start,end,location,attendees')
//         .orderby(`start/DateTime`)
//         .get();
//   }

module.exports = getGraphClient; //