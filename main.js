/*
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT License.
 */

const path = require("path");
const fs = require('fs');
const os = require('os');
const { app, ipcMain, BrowserWindow, ipcRenderer } = require("electron");
const { removeSpecialChar, formatAttendeesList } = require('./utils.js'); 

const AuthProvider = require("./App/AuthProvider");
const { IPC_MESSAGES } = require("./App/constants");
const { protectedResources, msalConfig } = require("./authConfig");
const getGraphClient = require("./App/graph");
const getCalendarEvents = require("./App/graph");
let authProvider;
let mainWindow;


function createWindow() {
    mainWindow = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: { 
            contextIsolation: true,
            preload: path.join(__dirname, "preload.js") },
    });
    mainWindow.maximize();
    authProvider = new AuthProvider(msalConfig);
}

// When app is ready load up the sign-in page 
app.on("ready", () => {
    createWindow();
    mainWindow.loadFile(path.join(__dirname, "./HTML/login.html")); 
});

app.on("window-all-closed", () => {
    app.quit();
});

app.on('activate', () => {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) {
        createWindow();
    }
});

ipcMain.on(IPC_MESSAGES.LOGIN, async () => {
// Event handlers for when a user logs in 
    const account = await authProvider.login();
    await mainWindow.loadFile(path.join(__dirname, "./HTML/dashboard.html"));    
    //display logged in user's name
    var nameAndEmail = {
      name : account.name,
      email : account.username,
    };
    mainWindow.webContents.send('pass_name_and_email', nameAndEmail);
    filename = removeSpecialChar(account.username)
    const jsonFile = fetchJson(filename);
    mainWindow.webContents.send('pass_json', jsonFile);
});

//logout behaviour
ipcMain.on(IPC_MESSAGES.LOGOUT, async () => {
  await authProvider.logout();
  mainWindow.loadFile(path.join(__dirname, "./HTML/login.html"));
});


/**
 * This functions fetch the json meeting note from 
 * local storage using a filename, makes it into an object and return it.
 * @param {*} userEmail User's email, where special symbols are removed
 * @returns A json object (empty json obj is file do not exist)
 */
function fetchJson(userEmail){
  userEmail = removeSpecialChar(userEmail);
  userEmail = userEmail+".json";
  var userDataPath = app.getPath('userData');
  var filePath = path.join(userDataPath, userEmail);
  try {
    var data = fs.readFileSync(filePath, 'utf8');
    const jsonData = JSON.parse(data);
    return jsonData;
  } catch(err) {
    console.error(`File does not exist on disk: ${err}`);
    return [];
  }   
}


/**Below code gets user's next meeting detail and 
 * send it to the front end for auto population 
*/
ipcMain.on(IPC_MESSAGES.GET_PROFILE, async () => {
    console.log("Invoked");
    const tokenRequest = {
        scopes: protectedResources.graphMe.scopes
    };

    const tokenResponse = await authProvider.getToken(tokenRequest);
    const account = authProvider.account;

    //get the next meeting - only within the next 7 days
    var dateNow = new Date();
    var dateNextWeek = new Date();
    dateNextWeek.setDate(dateNextWeek.getDate() + 7);
    

    //https://graph.microsoft.com/v1.0/me/calendarview?startdatetime=2023-05-13T00:46:51.909Z&enddatetime=2023-05-20T00:46:51.909Z
    const query = `startdatetime=${dateNow.toISOString()}&enddatetime=${dateNextWeek.toISOString()}`;
    try {
        const graphResponse = await getGraphClient(tokenResponse.accessToken)
        .api('https://graph.microsoft.com/v1.0/me/calendarview?'+query).get();

        if (graphResponse.value[0] != []) {
            
            //
            //note that 00:00 is added to preserve UTC timezone for conversions
            const dateTime = graphResponse.value[0].start.dateTime + '+00:00';
            const endTime = graphResponse.value[0].end.dateTime + '+00:00';
    
            //parsing them as dates to add hours
            const updatedDateTime = new Date(dateTime);
            const updatedEndTime = new Date(endTime);
    
            //adding 10 hours to match Canberra Timezone
            updatedDateTime.setHours(updatedDateTime.getHours() + 10);
            updatedEndTime.setHours(updatedEndTime.getHours() + 10);
    
            //returning as string value in ISO date/time format
            const updatedDateTimeString = updatedDateTime.toISOString();
    
            //string operations to truncate and space date and time extraction
            const truncDT = updatedDateTimeString.substring(0, updatedDateTimeString.length-8);
            //adding space between date and time
            const spacedDT = truncDT.slice(0, 10) + " " + truncDT.slice(10);
    
            //parsing start and end times as dates to perform time duration calculation
            const dT = Date.parse(updatedDateTime);
            const eT = Date.parse(updatedEndTime);
            const duration = eT - dT;
            //converting milliseconds to minutes
            const durationMins = duration/60000 + " Minutes";
    
            //attendees list formatting, separate each item in the list with ', ' and also add the word 'and' between the last two items of the list.
            var attendeesList = graphResponse.value[0].attendees.map((attendee) => attendee.emailAddress.name);
            attendeesList = formatAttendeesList(attendeesList);

            var meeting_details = {
              id : graphResponse.value[0].id, 
              title : graphResponse.value[0].subject,
              author : account.name, 
              attendees : attendeesList, 
              location : graphResponse.value[0].location.displayName, 
              date : spacedDT, 
              duration : durationMins,
              tags : []
            }
            
            // send meeting details to front end 
            mainWindow.webContents.send('new_note_auto_fill', meeting_details);                 
        } 

    } catch (error) {
        var meeting_details = {
          id : 0, 
          title : 0,
          author : 0, 
          attendees : 0, 
          location : 0, 
          date : 0, 
          duration : 0,
          tags : []
        }
        mainWindow.webContents.send('new_note_auto_fill', meeting_details);
    }
});



// /**
//  * 
//  * @param {*} email A user email encoded as string 
//  * @returns  Cleaned email address without speicial character @ and .
//  */
// function removeSpecialChar(email){
//   let cleanedEmail = "";
//   for (let i = 0; i < email.length; i++) {
//     const currentChar = email[i];
//     if (currentChar != "." && currentChar != "@"){
//       cleanedEmail += currentChar;
//     }
//   }
//   return cleanedEmail;
// }

/** 
 * Event handler for when front end sends a saving reqest to back end 
 * It writes the meeting note into storage as a json file
 * If it is a edit on existing note it will be replace
 * If it is the creation of a new note, it will be added to the end of the json file 
 * @param arg1 A dictionary that contain a single meeting notes details 
 * A meeting note (arg1) is defined as 
 *  id              : <Meeting id>
    currentEmail    : <Email address of the author> 
    currentTitle    : <Title of the meeting> 
    currentAuthor   : <Name of the author of the note>
    currentAttendee : <Attendee of the meeting>
    currentLocation : <Location of the meeting> 
    currentDate     : <Date of the meeting>
    currentDuration : <Duration of the meeting> 
    currentText     : <Content of the note?
 */
ipcMain.on('pass_file', (event, arg1) => {
    try {
      var newJsonObj = JSON.parse(arg1);
      var filename = newJsonObj.currentEmail;
      filename = removeSpecialChar(filename)
      filename += '.json';
      var userDataPath = app.getPath('userData');
      var filePath = path.join(userDataPath, filename);
  
      fs.access(filePath, fs.constants.F_OK, (err) => {
        if (!err) {
          fs.readFile(filePath, 'utf8', (err, data) => {
            if (err) throw err;
            // Parse the existing JSON data
            var existingData = JSON.parse(data);
            // If the existing data is not an array, make it an array
            if (!Array.isArray(existingData)) {
                existingData = [existingData];
            }
            // Find if a note with the same ID already exists
            const existingNoteIndex = existingData.findIndex(note => note.id === newJsonObj.id);
            if (existingNoteIndex !== -1) {
              // If it exists, replace it
              existingData[existingNoteIndex] = newJsonObj;
            } else {
              // If it doesn't exist, append the new note
              existingData.push(newJsonObj);
            }
            // Write the updated array back to the file
            fs.writeFile(filePath, JSON.stringify(existingData, null, 2), (err) => {
              if (err) throw err;
              console.log(`File updated.`);
            });
          });
        } 
        else {
          // If the file doesn't exist, create a new array with the new object
          fs.writeFile(filePath, JSON.stringify([newJsonObj], null, 2), (err) => {
            if (err) throw err;
          });
        }
      });
    } catch (err) {
      console.log(err);
      return;
    }
  });


  ipcMain.on('delete_note', (event, emailAndnoteID) => { 
    try {
        var filename = emailAndnoteID.currentEmail;
        filename = removeSpecialChar(filename)
        filename += '.json';
        var userDataPath = app.getPath('userData');
        var filePath = path.join(userDataPath, filename);
    
        fs.access(filePath, fs.constants.F_OK, (err) => {
            if (!err) {
                fs.readFile(filePath, 'utf8', (err, data) => {
                if (err) throw err;
                var existingData = JSON.parse(data);
                
                // Find the index of the note with the given ID
                const noteIndexToDelete = existingData.findIndex(note => note.id === emailAndnoteID.id);
                if (noteIndexToDelete !== -1) {
                    // Remove the note from the array
                    existingData.splice(noteIndexToDelete, 1);
                    
                    // Write the updated array back to the file
                    fs.writeFile(filePath, JSON.stringify(existingData, null, 2), (err) => {
                    if (err) throw err;
                    console.log(`Note deleted.`);
                    });
                }
                });
            } 
            else {
                console.log(`File not found for deletion.`);
            }
            });
        } catch (err) {
        console.log(err);
        return;
        }
    });
