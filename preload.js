// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License

const { contextBridge, ipcRenderer } = require('electron');

// Used to pass username/email and meeting details to dashboard.js renderer
contextBridge.exposeInMainWorld('electronAPI', {
    passNameandEmail: (callback) => ipcRenderer.on('pass_name_and_email', callback),
    newNoteAutoFill: (callback) => ipcRenderer.on('new_note_auto_fill', callback),
    passJson: (callback) => ipcRenderer.on('pass_json', callback),
    passFile: (callback) => ipcRenderer.send('pass_file', callback),
    deleteNote: (callback) => ipcRenderer.send('delete_note', callback),
  });
/**
 * This preload script exposes a "renderer" API to give
 * the Renderer process controlled access to some Node APIs
 * by leveraging IPC channels that have been configured for
 * communication between the Main and Renderer processes.
 */
contextBridge.exposeInMainWorld('renderer', {
    sendLoginMessage: () => {
        ipcRenderer.send('LOGIN');
    },
    sendSignoutMessage: () => {
        ipcRenderer.send('LOGOUT');
    },
    sendGetNextMeetingDetailMessage: () => {
        ipcRenderer.send('GET_PROFILE');
    },
    handleProfileData: (func) => {
        ipcRenderer.on('SET_PROFILE', (event, ...args) => func(event, ...args));
    },
    showWelcomeMessage: (func) => {
        ipcRenderer.on('SHOW_WELCOME_MESSAGE', (event, ...args) => func(event, ...args));
    },
});