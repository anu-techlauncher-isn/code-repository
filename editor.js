var quill = new Quill('#editor', {
    theme: 'snow',
    placeholder:'This is a new blank note page, type to edit it',
    modules: {
        toolbar: [
          ['bold', { 'background': [] }, 'italic', {'color': [] }],
          ['link', 'blockquote', 'code-block', 'image'],
          [{ list: 'ordered' }, { list: 'bullet' }]
        ]
      }

  });

