const { expect } = require('chai');
const path = require("path");
const { removeSpecialChar } = require('../utils.js');

describe('Email Cleaning', function () {
  it('should remove @ and . characters from email strings', function () {
    const email = 'john.doe@example.com';
    const cleanedEmail = removeSpecialChar(email);
    expect(cleanedEmail).to.equal('johndoeexamplecom');
  });

  it('should return an empty string for an empty email string', function () {
    const email = '';
    const cleanedEmail = removeSpecialChar(email);
    expect(cleanedEmail).to.equal('');
  });

  it('should handle email strings with no @ and . characters', function () {
    const email = 'johnDoe';
    const cleanedEmail = removeSpecialChar(email);
    expect(cleanedEmail).to.equal('johnDoe');
  });

  it('should return all other symbols except @ and .', function () {
    const email = '10aAzZ!@#$%^&*()_-+=`~{}[]|\\;:\'\",<.>?/';
    const cleanedEmail = removeSpecialChar(email);
    expect(cleanedEmail).to.equal('10aAzZ!#$%^&*()_-+=`~{}[]|\\;:\'\",<>?/');
  });

  it('remove whitespace', function () {
    const email = 'john doe@example.   com';
    const cleanedEmail = removeSpecialChar(email);
    expect(cleanedEmail).to.equal('johndoeexamplecom');
  });


  it('should process all ASCII characters correctly', function () {
    // Generate a string containing all byte characters 0 - 256
    let allChars = '';
    for (let i = 0; i <= 256; i++) {
      allChars += String.fromCharCode(i);
    }
    const cleaned = removeSpecialChar(allChars);
    // The expected result should be the same string but without '@' and '.'
    const expected = allChars.replace(/[@.]/g, '');
    expect(cleaned).to.equal(expected);
  });
});
