const { expect } = require('chai');
const { formatAttendeesList } = require('../utils');

describe('formatAttendeesList', function () {
  
  it('should format list with three names correctly', function () {
    const attendeesList = ['John', 'Jane', 'Sam'];
    expect(formatAttendeesList(attendeesList)).to.equal('John, Jane, and Sam');
  });

  it('should format list with two names correctly', function () {
    const attendeesList = ['John', 'Jane'];
    expect(formatAttendeesList(attendeesList)).to.equal('John, and Jane');
  });

  it('should return the single name if there is only one attendee', function () {
    const attendeesList = ['John'];
    expect(formatAttendeesList(attendeesList)).to.equal('John');
  });

  it('should return an empty string if there are no attendees', function () {
    const attendeesList = [];
    expect(formatAttendeesList(attendeesList)).to.equal('');
  });
});
