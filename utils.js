
/**
 * FUNCTIONS FOR UNIT TESTING
*/


/**
 * @param {*} email A user email encoded as string 
 * @returns  Cleaned email address without speicial character @ and .
 */
function removeSpecialChar(email){
    let cleanedEmail = "";
    for (let i = 0; i < email.length; i++) {
      const currentChar = email[i];
      if (currentChar != "." && currentChar != "@"){
        cleanedEmail += currentChar;
      }
    }
    return cleanedEmail;
  }


  function formatAttendeesList(attendeesList) {
    return attendeesList.join(", ").replace(/, ([^,]*)$/, ' and $1');
  }



/**
   * EXPORTS
*/
  module.exports = {
    removeSpecialChar,
    formatAttendeesList
  };