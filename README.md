# Code Repository

## Project Overview

Integrated Smart Notes (ISN) is a note-taking app designed to enhance and organise the meeting experience in a business setting. Through the use of automation, ISN can automatically populate various fields found in a meeting note (i.e., attendees and Time/Date/Location) of meetings. ISN targets to save time on finding notes/key items for meetings and to provide a single access point for all notes and/or saved material for a meeting.
The application will launch on three major operating systems, which are Windows, MacOS and Linux. Additionally, the app must interact with Microsoft Outlook Calendars to pull meeting details used for meeting notes auto-population and online notes saving. The user interface will feature a simple design with appropriate themes and colours, aiming to be intuitive for the user and easy to navigate. These design elements will allow for a smooth user experience, ultimately leading to a more efficient note-taking process.

## Tech-Stack Introduction

Integrated Smart Notes (ISN) is a desktop application created by using the popular framework [Electron](https://www.electronjs.org/). Electron allows developers to create cross-platform desktop applications using web technologies such as HTML, CSS, and JavaScript. It was originally developed by GitHub and has since been adopted by many other companies and developers. The Electron framework is based on the Node.js runtime and the Chromium web browser engine, which allows developers to build desktop applications that look and feel like native applications, but are built using web technologies. With Electron, developers can create applications for Windows, macOS, and Linux using a single codebase.
`<br>`
The backend of the application is handled using Node.js, which is a runtime environment that allows developers to run JavaScript code outside of a web browser. Node.js provides access to the operating system's file system, networking, and other low-level functionality, which makes it a powerful tool for building backend functionality for desktop applications.

## Installation guide

1. Install [npm and node.js](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm).
2. Verify installation of npm and node.js with the following commands

```
node -v
npm -v
```

3. Install Electron with the following command

```
npm install --save-dev electron
```

## Starting the program

To start the program, change the directory to the root directory of the project then use the following commands

```
npm start
```

Alternatively, install the application via the installation file that can be found in the root directory of the repository.

## Tech-Stack decision rational

Electron frame along with Node.js was chosen as the Tech-Stack due to the following reasons:

1. Familiar technologies: Electron allows developers to use familiar web technologies like HTML, CSS, and JavaScript to build desktop applications. All members of the team have prior experience working with web-based frameworks. Electron will simplify GUI development processes.
2. Large community and ecosystem: Electron has a large and active community of developers who contribute to its development, create plugins and packages, and provide support. This ensures guides are readily available online for both learning and trouble-shooting.
3. Cross-platform development: Electron allows developers to build cross-platform desktop applications that can run on Windows, macOS, and Linux. Which satisfies our client's requirements.
4. Proven framework: Large applications such as VS code and discord are built using the Electron framework, thus proving that it is capable of producing an app that fits our purpose.
5. Low Learning Curve: Both Node.js and Electron uses popular modern technologies with various guide and low complexity thus leading to a low learning curve.

A list of Tech-Stacks considered along with rationales can be found [here](https://drive.google.com/file/d/1w0oK8wJDmx-j533w97sNrcgaNw1LTYbt/view?usp=sharing).
